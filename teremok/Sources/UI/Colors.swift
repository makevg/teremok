//
//  Colors.swift
//  teremok
//
//  Created by Максимычев Е.О. on 26/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

struct Colors {
	static let dark = UIColor(red: 0.23, green: 0.25, blue: 0.28, alpha: 1)
	static let brown = UIColor(red: 0.37, green: 0.32, blue: 0.31, alpha: 1)
	static let lightGray = UIColor(red: 0.76, green: 0.76, blue: 0.76, alpha: 1)
	static let red = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
}
