//
//  BaseViewController.swift
//  teremok
//
//  Created by Максимычев Е.О. on 26/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
	
	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupUI()
	}
	
	// MARK: - Configure
	
	func setupUI() {
		
	}

}
