//
//  SignUpViewController.swift
//  teremok
//
//  Created by Maximychev Evgeny on 24/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class SignUpViewController: ModalViewController {
	
	// MARK: - Properties
	
	private lazy var signUpView: UIView = {
		let view = SignUpContainerView()
		view.backButtonIsHidden = true
		view.signInButton.addTarget(
			self,
			action: #selector(signInButtonTapped),
			for: .touchUpInside)
		let text = StyledText(
			text: localizedText(for: .enterPhoneNumber),
			style: TextStyle(
				color: Colors.lightGray,
				font: .systemFont(ofSize: 14)))
		view.infoView.infoLabel.set(styledText: text)
		
		return view
	}()

	// MARK: - Override
	
	override func setupUI() {
		super.setupUI()
		
		addContentView(contentView: signUpView)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.setNavigationBarHidden(true, animated: animated)
	}
	
	// MARK: - Actions
	
	@objc private func signInButtonTapped() {
		navigationController?.pushViewController(SignUpSmsCodeViewController(), animated: false)
	}

}

fileprivate final class SignUpContainerView: LoginView {
	
	private struct Appearance {
		static let infoViewHeightAnchorConstant: CGFloat = 175
		static let phoneFieldHeightAnchorConstant: CGFloat = 60
		static let phoneFieldWidthAnchorConstant: CGFloat = 300
		static let phoneFieldTopAnchorConstant: CGFloat = 10
		static let signInButtonHeightAnchorConstant: CGFloat = 50
		static let signInButtonWidthAnchorConstant: CGFloat = 300
		static let signInButtonTopAnchorConstant: CGFloat = 24
		static let personalDataButtonHeightAnchorConstant: CGFloat = 28
		static let personalDataButtonWidthAnchorConstant: CGFloat = 295
		static let personalDataButtonTopAnchorConstant: CGFloat = 16
		static let hasAccountButtonHeightAnchorConstant: CGFloat = 50
		static let hasAccountButtonWidthAnchorConstant: CGFloat = 300
		static let hasAccountButtonBottomAnchorConstant: CGFloat = -30
	}
	
	// MARK: - Properties
	
	lazy var phoneField: PhoneField = {
		return PhoneField()
	}()
	
	lazy var signInButton: RoundedButton = {
		let button = RoundedButton()
		button.set(styledText: StyledText(
			text: localizedText(for: .signInButtonTitle),
			style: TextStyle(color: .white, font: .systemFont(ofSize: 15))))
		
		return button
	}()
	
	lazy var personalDataButton: UIButton = {
		let button = UIButton()
		button.titleLabel?.numberOfLines = 0
		button.titleLabel?.textAlignment = .center
		button.titleLabel?.lineBreakMode = .byWordWrapping
		button.set(styledText: StyledText(
			text: localizedText(for: .acceptPersonalDataProcessing),
			style: TextStyle(
				color: Colors.lightGray,
				font: .systemFont(ofSize: 12))))
		
		return button
	}()
	
	lazy var hasAccountButton: UIButton = {
		let button = UIButton()
		button.set(styledText: StyledText(
			text: localizedText(for: .hasAccount),
			style: TextStyle(
				color: Colors.brown,
				font: .systemFont(ofSize: 15))))
		
		return button
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		prepareLayout()
	}
	
	private func prepareLayout() {
		addSubviews(
			[phoneField,
			 signInButton,
			 personalDataButton,
			 hasAccountButton])
		
		NSLayoutConstraint.activate(
			[phoneField.heightAnchor.constraint(
				equalToConstant: Appearance.phoneFieldHeightAnchorConstant),
			 phoneField.widthAnchor.constraint(
				equalToConstant: Appearance.phoneFieldWidthAnchorConstant),
			 phoneField.topAnchor.constraint(
				equalTo: infoView.bottomAnchor,
				constant: Appearance.phoneFieldTopAnchorConstant),
			 phoneField.centerXAnchor.constraint(
				equalTo: infoView.centerXAnchor),
			 signInButton.heightAnchor.constraint(
				equalToConstant: Appearance.signInButtonHeightAnchorConstant),
			 signInButton.widthAnchor.constraint(
				equalToConstant: Appearance.signInButtonWidthAnchorConstant),
			 signInButton.topAnchor.constraint(
				equalTo: phoneField.bottomAnchor,
				constant: Appearance.signInButtonTopAnchorConstant),
			 signInButton.centerXAnchor.constraint(
				equalTo: phoneField.centerXAnchor),
			 personalDataButton.heightAnchor.constraint(
				equalToConstant: Appearance.personalDataButtonHeightAnchorConstant),
			 personalDataButton.widthAnchor.constraint(
				equalToConstant: Appearance.personalDataButtonWidthAnchorConstant),
			 personalDataButton.topAnchor.constraint(
				equalTo: signInButton.bottomAnchor,
				constant: Appearance.personalDataButtonTopAnchorConstant),
			 personalDataButton.centerXAnchor.constraint(
				equalTo: signInButton.centerXAnchor),
			 hasAccountButton.heightAnchor.constraint(
				equalToConstant: Appearance.hasAccountButtonHeightAnchorConstant),
			 hasAccountButton.widthAnchor.constraint(
				equalToConstant: Appearance.hasAccountButtonWidthAnchorConstant),
			 hasAccountButton.centerXAnchor.constraint(
				equalTo: centerXAnchor),
			 hasAccountButton.bottomAnchor.constraint(
				equalTo: bottomAnchor,
				constant: Appearance.hasAccountButtonBottomAnchorConstant)])
	}
	
}
