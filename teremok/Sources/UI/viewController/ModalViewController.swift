//
//  ModalViewController.swift
//  teremok
//
//  Created by Maximychev Evgeny on 24/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

class ModalViewController: BaseViewController {
	
	private struct Appearance {
		static let containerViewHeightAnchorConstant: CGFloat = 530
	}
	
	// MARK: - Properties
	
	private lazy var backgroundView: UIView = {
		let view = UIView()
		view.backgroundColor = .black
		view.alpha = 0.6
		let tapGestureRecognizer = UITapGestureRecognizer(
			target: self,
			action: #selector(viewTapped))
		view.addGestureRecognizer(tapGestureRecognizer)
		
		return view
	}()
	
	private lazy var containerView: UIView = {
		let view = UIView()
		view.backgroundColor = .white
		view.layer.cornerRadius = 35
		
		return view
	}()
	
	// MARK: - Override
	
	override func setupUI() {
		super.setupUI()
		
		view.addSubviews([backgroundView, containerView])
		NSLayoutConstraint.activate([
			backgroundView.topAnchor.constraint(
				equalTo: view.topAnchor),
			backgroundView.bottomAnchor.constraint(
				equalTo: view.bottomAnchor),
			backgroundView.leadingAnchor.constraint(
				equalTo: view.leadingAnchor),
			backgroundView.trailingAnchor.constraint(
				equalTo: view.trailingAnchor),
			containerView.heightAnchor.constraint(
				equalToConstant: Appearance.containerViewHeightAnchorConstant),
			containerView.leadingAnchor.constraint(
				equalTo: view.leadingAnchor),
			containerView.trailingAnchor.constraint(
				equalTo: view.trailingAnchor),
			containerView.bottomAnchor.constraint(
				equalTo: view.bottomAnchor)])
	}
	
	// MARK: - Actions
	
	@objc private func viewTapped() {
		dismiss(animated: true, completion: nil)
	}
	
	// MARK: - Configure
	
	func addContentView(contentView: UIView) {
		containerView.addSubviews([contentView])
		
		NSLayoutConstraint.activate(
			[contentView.topAnchor.constraint(equalTo: containerView.topAnchor),
			 contentView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
			 contentView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
			 contentView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)])
	}

}
