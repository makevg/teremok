//
//  AlertViewController.swift
//  teremok
//
//  Created by Maximychev Evgeny on 25/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class AlertViewController: BaseViewController {
	
	private struct Appearance {
		static let alertViewHeightAnchorConstant: CGFloat = 265
		static let alertViewWidthAnchorConstant: CGFloat = 270
	}
	
	// MARK: - Properties
	
	var alertTitle: String = "" {
		didSet { alertView.titleLabel.text = alertTitle }
	}
	
	var alertDescription: String = "" {
		didSet { alertView.descriptionLabel.text = alertDescription }
	}
	
	var buttonTitle: String = "" {
		didSet { alertView.button.setTitle(buttonTitle, for: .normal) }
	}
	
	var buttonActionHandler: (() -> Void)?
	
	private lazy var backgroundView: UIView = {
		let view = UIView()
		view.backgroundColor = .black
		view.alpha = 0.6
		let tapGestureRecognizer = UITapGestureRecognizer(
			target: self,
			action: #selector(viewTapped))
		view.addGestureRecognizer(tapGestureRecognizer)
		
		return view
	}()
	
	private lazy var alertView: AlertView = {
		let alert = AlertView()
		alert.button.addTarget(
			self,
			action: #selector(buttonTapped),
			for: .touchUpInside)
		
		return alert
	}()
	
	// MARK: - Override
	
	override func setupUI() {
		super.setupUI()
		
		view.addSubviews([backgroundView, alertView])
		
		NSLayoutConstraint.activate([
			backgroundView.topAnchor.constraint(
				equalTo: view.topAnchor),
			backgroundView.bottomAnchor.constraint(
				equalTo: view.bottomAnchor),
			backgroundView.leadingAnchor.constraint(
				equalTo: view.leadingAnchor),
			backgroundView.trailingAnchor.constraint(
				equalTo: view.trailingAnchor),
			alertView.heightAnchor.constraint(
				equalToConstant: Appearance.alertViewHeightAnchorConstant),
			alertView.widthAnchor.constraint(
				equalToConstant: Appearance.alertViewWidthAnchorConstant),
			alertView.centerYAnchor.constraint(
				equalTo: view.centerYAnchor),
			alertView.centerXAnchor.constraint(
				equalTo: view.centerXAnchor)])
	}
	
	// MARK: - Actions
	
	@objc private func viewTapped() {
		dismiss(animated: true, completion: nil)
	}
	
	@objc private func buttonTapped() {
		buttonActionHandler?()
	}

}

fileprivate final class AlertView: BaseView {
	
	private struct Appearance {
		static let logoViewHeightConstraint: CGFloat = 95
		static let titleLabelTopAnchorConstant: CGFloat = 4
		static let titleLabelLeadingAnchorConstant: CGFloat = 16
		static let titleLabelTrailingAnchorConstant: CGFloat = -16
		static let descriptionLabelTopAnchorConstant: CGFloat = 4
		static let descriptionLabelBottomAnchorConstant: CGFloat = -4
		static let lineHeightAnchorConstant: CGFloat = 0.5
		static let buttonHeightAnchorConstant: CGFloat = 50
	}
	
	// MARK: - Properties
	
	private lazy var logoView: LogoView = {
		return LogoView()
	}()
	
	lazy var titleLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .center
		label.set(textStyle: TextStyle(
			color: .black,
			font: .systemFont(ofSize: 17, weight: .medium)))
		
		return label
	}()
	
	lazy var descriptionLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 0
		label.textAlignment = .center
		label.set(textStyle: TextStyle(
			color: .black,
			font: .systemFont(ofSize: 13)))
		
		return label
	}()
	
	private lazy var line: UIView = {
		let line = UIView()
		line.backgroundColor = .lightGray
		
		return line
	}()
	
	lazy var button: UIButton = {
		let button = UIButton()
		button.set(textStyle: TextStyle(
			color: Colors.brown,
			font: .systemFont(ofSize: 17, weight: .medium)))
		
		return button
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		backgroundColor = .white
		layer.cornerRadius = 14
		
		addSubviews(
			[logoView,
			 titleLabel,
			 descriptionLabel,
			 line,
			 button])
		
		NSLayoutConstraint.activate(
			[logoView.heightAnchor.constraint(
				equalToConstant: Appearance.logoViewHeightConstraint),
			 logoView.topAnchor.constraint(
				equalTo: topAnchor),
			 logoView.leadingAnchor.constraint(
				equalTo: leadingAnchor),
			 logoView.trailingAnchor.constraint(
				equalTo: trailingAnchor),
			 titleLabel.topAnchor.constraint(
				equalTo: logoView.bottomAnchor,
				constant: Appearance.titleLabelTopAnchorConstant),
			 titleLabel.leadingAnchor.constraint(
				equalTo: leadingAnchor,
				constant: Appearance.titleLabelLeadingAnchorConstant),
			 titleLabel.trailingAnchor.constraint(
				equalTo: trailingAnchor,
				constant: Appearance.titleLabelTrailingAnchorConstant),
			 descriptionLabel.topAnchor.constraint(
				equalTo: titleLabel.bottomAnchor,
				constant: Appearance.descriptionLabelTopAnchorConstant),
			 descriptionLabel.bottomAnchor.constraint(
				equalTo: line.topAnchor,
				constant: Appearance.descriptionLabelBottomAnchorConstant),
			 descriptionLabel.leadingAnchor.constraint(
				equalTo: titleLabel.leadingAnchor),
			 descriptionLabel.trailingAnchor.constraint(
				equalTo: titleLabel.trailingAnchor),
			 line.heightAnchor.constraint(
				equalToConstant: Appearance.lineHeightAnchorConstant),
			 line.leadingAnchor.constraint(
				equalTo: leadingAnchor),
			 line.trailingAnchor.constraint(
				equalTo: trailingAnchor),
			 line.bottomAnchor.constraint(
				equalTo: button.topAnchor),
			 button.heightAnchor.constraint(
				equalToConstant: Appearance.buttonHeightAnchorConstant),
			 button.bottomAnchor.constraint(
				equalTo: bottomAnchor),
			 button.leadingAnchor.constraint(
				equalTo: leadingAnchor),
			 button.trailingAnchor.constraint(
				equalTo: trailingAnchor)])
	}
	
}
