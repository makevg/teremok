//
//  TermsOfUseViewController.swift
//  teremok
//
//  Created by Maximychev Evgeny on 26/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class TermsOfUseViewController: ModalViewController {
	
	// MARK: - Properties
	
	private lazy var termsOfUseView: TermsOfUseView = {
		let view = TermsOfUseView()
		view.acceptButton.addTarget(
			self,
			action: #selector(acceptButtonTapped),
			for: .touchUpInside)
		
		return view
	}()
	
	// MARK: - Override
	
	override func setupUI() {
		super.setupUI()
		
		addContentView(contentView: termsOfUseView)
	}
	
	// MARK: - Actions
	
	@objc private func acceptButtonTapped() {
		let alert = AlertViewController()
		alert.alertTitle = "Добро пожаловать!"
		alert.alertDescription = "Здесь можно быстро и удобно найти или сдать жилье, познакомиться с гостем и хозяином до встречи, а также поделиться друг с другом впечатлениями и интересами."
		alert.buttonTitle = "Приступить"
		navigationController?.viewControllers = [alert]
	}

}

fileprivate final class TermsOfUseView: BaseView {
	
	private struct Appearance {
		static let titleLabelTopAnchorConstant: CGFloat = 28
		static let titleLabelLeadingAnchorConstant: CGFloat = 36
		static let titleLabelTrailingAnchorConstant: CGFloat = -36
		static let textViewTopAnchorConstant: CGFloat = 21
		static let textViewBottomAnchorConstant: CGFloat = -29
		static let acceptButtonHeightAnchorConstant: CGFloat = 50
		static let acceptButtonWidthAnchorConstant: CGFloat = 300
		static let acceptButtonBottomAnchorConstant: CGFloat = -30
	}
	
	// MARK: - Private
	
	private lazy var titleLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 2
		label.set(styledText: StyledText(
			text: localizedText(for: .termsOfUseTitle),
			style: TextStyle(
				color: Colors.dark,
				font: .systemFont(ofSize: 28, weight: .bold))))
		
		return label
	}()
	
	private lazy var textView: UITextView = {
		let textView = UITextView()
		textView.isEditable = false
		textView.set(styledText: StyledText(
			text: localizedText(for: .termsOfUseDescription),
			style: TextStyle(
				color: Colors.lightGray,
				font: .systemFont(ofSize: 15))))
		
		return textView
	}()
	
	lazy var acceptButton: RoundedButton = {
		let button = RoundedButton()
		button.set(styledText: StyledText(
			text: localizedText(for: .termsOfUseAcceptButtonTitle),
			style: TextStyle(
				color: .white,
				font: .systemFont(ofSize: 15))))
		
		return button
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		addSubviews([titleLabel, textView, acceptButton])
		
		NSLayoutConstraint.activate(
			[titleLabel.topAnchor.constraint(
				equalTo: topAnchor,
				constant: Appearance.titleLabelTopAnchorConstant),
			 titleLabel.leadingAnchor.constraint(
				equalTo: leadingAnchor,
				constant: Appearance.titleLabelLeadingAnchorConstant),
			 titleLabel.trailingAnchor.constraint(
				equalTo: trailingAnchor,
				constant: Appearance.titleLabelTrailingAnchorConstant),
			 textView.topAnchor.constraint(
				equalTo: titleLabel.bottomAnchor,
				constant: Appearance.textViewTopAnchorConstant),
			 textView.leadingAnchor.constraint(
				equalTo: titleLabel.leadingAnchor),
			 textView.trailingAnchor.constraint(
				equalTo: titleLabel.trailingAnchor),
			 textView.bottomAnchor.constraint(
				equalTo: acceptButton.topAnchor,
				constant: Appearance.textViewBottomAnchorConstant),
			 acceptButton.heightAnchor.constraint(
				equalToConstant: Appearance.acceptButtonHeightAnchorConstant),
			 acceptButton.widthAnchor.constraint(
				equalToConstant: Appearance.acceptButtonWidthAnchorConstant),
			 acceptButton.centerXAnchor.constraint(
				equalTo: centerXAnchor),
			 acceptButton.bottomAnchor.constraint(
				equalTo: bottomAnchor,
				constant: Appearance.acceptButtonBottomAnchorConstant)])
	}
	
}
