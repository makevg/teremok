//
//  HomeViewController.swift
//  teremok
//
//  Created by Maximychev Evgeny on 20/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit
import AccountKit

fileprivate struct PageViewModel {
	let title: StyledText
	let description: StyledText
	let image: String
}

final class HomeViewController: BaseViewController {
	
	private struct Appearance {
		static let pageControlBottomAnchorConstant: CGFloat = -24
		static let loginButtonsBarHeightAnchorConstant: CGFloat = 170
	}
	
	// MARK: - Properties
	
	private lazy var accountKitManager: AccountKitManager = {
		return AccountKitManager(responseType: .accessToken)
	}()
	
	private lazy var pageViewController: UIPageViewController = {
		let pageVC = UIPageViewController(
			transitionStyle: .scroll,
			navigationOrientation: .horizontal,
			options: nil)
		pageVC.dataSource = self
		pageVC.delegate = self
		
		return pageVC
	}()
	
	private lazy var pageControl: UIPageControl = {
		let pageControl = UIPageControl()
		pageControl.currentPage = 0
		pageControl.numberOfPages = pageViewModels.count
		pageControl.pageIndicatorTintColor = .gray
		pageControl.currentPageIndicatorTintColor = .white
		
		return pageControl
	}()
	
	private lazy var loginButtonsBar: UIView = {
		let buttonsBar = LoginButtonsBar()
		buttonsBar.signInButtonActionHandler = {
			self.showSignIn()
		}
		buttonsBar.signUpButtonActionHandler = {
			self.showSignUp()
		}
		
		return buttonsBar
	}()
	
	private lazy var pageViewModels: [PageViewModel] = {
		let titleFont = UIFont.systemFont(ofSize: 36)
		let descriptionFont = UIFont.systemFont(ofSize: 18)
		
		return [PageViewModel(
			title: StyledText(
				text: localizedText(for: .firstHomePageTitle),
				style: TextStyle(color: .white, font: titleFont)),
			description: StyledText(
				text: localizedText(for: .firstHomePageDescription),
				style: TextStyle(color: .white, font: descriptionFont)),
			image: "Slide1"),
				PageViewModel(
					title: StyledText(
						text: localizedText(for: .secondHomePageTitle),
						style: TextStyle(color: .white, font: titleFont)),
					description: StyledText(
						text: localizedText(for: .secondHomePageDescription),
						style: TextStyle(color: .white, font: descriptionFont)),
					image: "Slide2"),
				PageViewModel(
					title: StyledText(
						text: localizedText(for: .thirdHomePageTitle),
						style: TextStyle(color: .black, font: titleFont)),
					description: StyledText(
						text: localizedText(for: .thirdHomePageDescription),
						style: TextStyle(color: .black, font: descriptionFont)),
					image: "Slide3")]
	}()
	
	// MARK: - Override
	
	override func setupUI() {
		super.setupUI()
		
		view.backgroundColor = .white
		setupPageViewController()
		
		view.addSubviews([pageControl, loginButtonsBar])
		NSLayoutConstraint.activate(
			[pageControl.centerXAnchor.constraint(
				equalTo: view.centerXAnchor),
			 pageControl.bottomAnchor.constraint(
				equalTo: loginButtonsBar.topAnchor,
				constant: Appearance.pageControlBottomAnchorConstant),
			 loginButtonsBar.heightAnchor.constraint(
				equalToConstant: Appearance.loginButtonsBarHeightAnchorConstant),
			 loginButtonsBar.leadingAnchor.constraint(
				equalTo: view.leadingAnchor),
			 loginButtonsBar.trailingAnchor.constraint(
				equalTo: view.trailingAnchor),
			 loginButtonsBar.bottomAnchor.constraint(
				equalTo: view.bottomAnchor)])
	}
	
	private func setupPageViewController() {
		addChild(pageViewController)
		view.addSubview(pageViewController.view)
		pageViewController.view.frame = view.bounds
		pageViewController.didMove(toParent: self)
		
		pageViewController.setViewControllers(
			[getPageContentViewController(at: 0)],
			direction: .forward,
			animated: true,
			completion: nil)
	}
	
	private func getPageContentViewController(at index: Int) -> PageContentViewController {
		return PageContentViewController(
			pageIndex: index,
			viewModel: pageViewModels[index])
	}
	
	private func showModalViewController(viewController: UIViewController) {
		viewController.modalTransitionStyle = .crossDissolve
		viewController.modalPresentationStyle = .overCurrentContext
		present(viewController, animated: true, completion: nil)
	}

	private func showSignIn() {
//		let navVC = UINavigationController(rootViewController: SignInViewController())
//		showModalViewController(
//			viewController: navVC)
		
		showAccountKitViewController()
	}
	
	private func showSignUp() {
		let navVC = UINavigationController(rootViewController: SignUpViewController())
		showModalViewController(
			viewController: navVC)
		
	}
	
	private func showAccountKitViewController() {
		let vc = accountKitManager.viewControllerForPhoneLogin()
		vc.delegate = self
		vc.defaultCountryCode = "RU"
		
		let skinManager = SkinManager(
			skinType: .classic,
			primaryColor: Colors.brown,
			backgroundImage: nil,
			backgroundTint: .white,
			tintIntensity: 0)
		vc.uiManager = skinManager
		
		present(vc, animated: true, completion: nil)
	}
	
}

// MARK: - Extensions

// MARK: - UIPageViewControllerDataSource

extension HomeViewController: UIPageViewControllerDataSource {
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		var index = (viewController as! PageContentViewController).pageIndex
		
		if index == 0 || index == NSNotFound {
			return nil
		}
		
		index-=1
		
		return getPageContentViewController(at: index)
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		var index = (viewController as! PageContentViewController).pageIndex
		
		if index == NSNotFound {
			return nil
		}
		
		index+=1
		
		if index == pageViewModels.count {
			return nil
		}
		
		return getPageContentViewController(at: index)
	}
	
}

// MARK: - UIPageViewControllerDelegate

extension HomeViewController: UIPageViewControllerDelegate {
	
	func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
		let viewController = pageViewController.viewControllers?.first
		let pageIndex = (viewController as! PageContentViewController).pageIndex
		pageControl.currentPage = pageIndex
	}
	
}

fileprivate final class PageContentViewController: UIViewController {
	
	private struct Appearance {
		static let titleLabelTopAnchorConstant: CGFloat = 135
		static let titleLabelLeadingAnchorConstant: CGFloat = 20
		static let titleLabelTrailingAnchorConstant: CGFloat = -20
		static let descriptionLabelTopAnchorConstant: CGFloat = 16
	}
	
	// MARK: - Properties
	
	let pageIndex: Int
	let viewModel: PageViewModel
	
	private lazy var titleLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 3
		label.font = UIFont.systemFont(ofSize: 36)
		
		return label
	}()
	
	private lazy var descriptionLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 4
		label.font = UIFont.systemFont(ofSize: 18)
		
		return label
	}()
	
	private lazy var imageView: UIImageView = {
		let imageView = UIImageView()
		imageView.contentMode = .scaleAspectFill
		
		return imageView
	}()
	
	// MARK: - Init
	
	init(pageIndex: Int, viewModel: PageViewModel) {
		self.pageIndex = pageIndex
		self.viewModel = viewModel
		
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupUI()
		
		titleLabel.set(styledText: viewModel.title)
		descriptionLabel.set(styledText: viewModel.description)
		imageView.image = UIImage(named: viewModel.image)
	}
	
	// MARK: - Private
	
	private func setupUI() {
		view.addSubviews(
			[imageView, titleLabel, descriptionLabel])
		
		NSLayoutConstraint.activate(
			[imageView.topAnchor.constraint(
				equalTo: view.topAnchor),
			 imageView.bottomAnchor.constraint(
				equalTo: view.bottomAnchor),
			 imageView.leadingAnchor.constraint(
				equalTo: view.leadingAnchor),
			 imageView.trailingAnchor.constraint(
				equalTo: view.trailingAnchor),
			 titleLabel.topAnchor.constraint(
				equalTo: view.safeAreaLayoutGuide.topAnchor,
				constant: Appearance.titleLabelTopAnchorConstant),
			 titleLabel.leadingAnchor.constraint(
				equalTo: view.safeAreaLayoutGuide.leadingAnchor,
				constant: Appearance.titleLabelLeadingAnchorConstant),
			 titleLabel.trailingAnchor.constraint(
				equalTo: view.safeAreaLayoutGuide.trailingAnchor,
				constant: Appearance.titleLabelTrailingAnchorConstant),
			 descriptionLabel.topAnchor.constraint(
				equalTo: titleLabel.bottomAnchor,
				constant: Appearance.descriptionLabelTopAnchorConstant),
			 descriptionLabel.leadingAnchor.constraint(
				equalTo: titleLabel.leadingAnchor),
			 descriptionLabel.trailingAnchor.constraint(
				equalTo: titleLabel.trailingAnchor)])
	}
	
}

// MARK: - AKFViewControllerDelegate

extension HomeViewController: AKFViewControllerDelegate {
	
	func viewController(
		_ viewController: UIViewController & AKFViewController,
		didCompleteLoginWith code: String,
		state: String) {
		print("code: \(code)")
	}
	
	func viewController(
		_ viewController: UIViewController & AKFViewController,
		didCompleteLoginWith accessToken: AKFAccessToken,
		state: String) {
		print("accessToken: \(accessToken)")
	}
	
	func viewController(
		_ viewController: UIViewController & AKFViewController,
		didFailWithError error: Error) {
		print("error: \(error.localizedDescription)")
	}
	
}
