//
//  SignUpSmsCodeViewController.swift
//  teremok
//
//  Created by Maximychev Evgeny on 25/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class SignUpSmsCodeViewController: ModalViewController {

	// MARK: - Properties
	
	private lazy var signUpSmsCodeView: UIView = {
		let view = SignUpSmsCodeViewContainer()
		view.backButtonActionHandler = {
			self.navigationController?.popViewController(animated: false)
		}
		let text = StyledText(
			text: localizedText(for: .codeSendToNumber),
			style: TextStyle(
				color: Colors.lightGray,
				font: .systemFont(ofSize: 14)))
		view.infoView.infoLabel.set(styledText: text)
		view.smsCodeField.codeEnteredHandler = { code in
			print("Code entered: \(code)")
			self.navigationController?.pushViewController(
				SignUpSetPasswordViewController(),
				animated: false)
		}
		
		return view
	}()
	
	// MARK: - Override
	
	override func setupUI() {
		super.setupUI()
		
		addContentView(contentView: signUpSmsCodeView)
	}
	
}

fileprivate final class SignUpSmsCodeViewContainer: LoginView {
	
	private struct Appearance {
		static let infoViewHeightAnchorConstant: CGFloat = 175
		static let smsCodeFieldHeightAnchorConstant: CGFloat = 60
		static let smsCodeFieldWidthAnchorConstant: CGFloat = 229
		static let smsCodeFieldTopAnchorConstant: CGFloat = 10
		static let questionLabelTopAnchorConstant: CGFloat = 24
		static let resendCodeButtonHeightAnchorConstant: CGFloat = 17
		static let resendCodeButtonWidthAnchorConstant: CGFloat = 180
		static let resendCodeButtonTopAnchorConstant: CGFloat = 16
		static let hasAccountButtonHeightAnchorConstant: CGFloat = 50
		static let hasAccountButtonWidthAnchorConstant: CGFloat = 300
		static let hasAccountButtonBottomAnchorConstant: CGFloat = -30
	}
	
	// MARK: - Properties
	
	lazy var smsCodeField: SmsCodeField = {
		return SmsCodeField()
	}()
	
	private lazy var questionLabel: UILabel = {
		let label = UILabel()
		let text = StyledText(
			text: localizedText(for: .dontReceiveCode),
			style: TextStyle(
				color: .lightGray,
				font: .systemFont(ofSize: 14)))
		label.set(styledText: text)
		
		return label
	}()
	
	private lazy var resendCodeButton: UIButton = {
		let button = UIButton()
		button.set(styledText: StyledText(
			text: localizedText(for: .resendCode),
			style: TextStyle(
				color: Colors.brown,
				font: .systemFont(ofSize: 15))))
		
		return button
	}()
	
	private lazy var hasAccountButton: UIButton = {
		let button = UIButton()
		button.set(styledText: StyledText(
			text: localizedText(for: .hasAccount),
			style: TextStyle(
				color: Colors.brown,
				font: .systemFont(ofSize: 15))))
		
		return button
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		prepareLayout()
	}
	
	private func prepareLayout() {
		addSubviews(
			[smsCodeField,
			 questionLabel,
			 resendCodeButton,
			 hasAccountButton])
		
		NSLayoutConstraint.activate(
			[smsCodeField.heightAnchor.constraint(
				equalToConstant: Appearance.smsCodeFieldHeightAnchorConstant),
			 smsCodeField.widthAnchor.constraint(
				equalToConstant: Appearance.smsCodeFieldWidthAnchorConstant),
			 smsCodeField.topAnchor.constraint(
				equalTo: infoView.bottomAnchor,
				constant: Appearance.smsCodeFieldTopAnchorConstant),
			 smsCodeField.centerXAnchor.constraint(
				equalTo: infoView.centerXAnchor),
			 questionLabel.topAnchor.constraint(
				equalTo: smsCodeField.bottomAnchor,
				constant: Appearance.questionLabelTopAnchorConstant),
			 questionLabel.centerXAnchor.constraint(
				equalTo: smsCodeField.centerXAnchor),
			 resendCodeButton.heightAnchor.constraint(
				equalToConstant: Appearance.resendCodeButtonHeightAnchorConstant),
			 resendCodeButton.widthAnchor.constraint(
				equalToConstant: Appearance.resendCodeButtonWidthAnchorConstant),
			 resendCodeButton.topAnchor.constraint(
				equalTo: questionLabel.bottomAnchor,
				constant: Appearance.resendCodeButtonTopAnchorConstant),
			 resendCodeButton.centerXAnchor.constraint(
				equalTo: questionLabel.centerXAnchor),
			 hasAccountButton.heightAnchor.constraint(
				equalToConstant: Appearance.hasAccountButtonHeightAnchorConstant),
			 hasAccountButton.widthAnchor.constraint(
				equalToConstant: Appearance.hasAccountButtonWidthAnchorConstant),
			 hasAccountButton.centerXAnchor.constraint(
				equalTo: centerXAnchor),
			 hasAccountButton.bottomAnchor.constraint(
				equalTo: bottomAnchor,
				constant: Appearance.hasAccountButtonBottomAnchorConstant)])
	}
	
}
