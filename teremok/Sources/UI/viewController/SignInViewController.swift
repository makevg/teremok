//
//  SignInViewController.swift
//  teremok
//
//  Created by Maximychev Evgeny on 21/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class SignInViewController: ModalViewController {
	
	// MARK: - Properties
	
	private lazy var signInView: UIView = {
		let view = SignInContainerView()
		view.backButtonIsHidden = true
		
		return view
	}()
	
	// MARK: - Override
	
	override func setupUI() {
		super.setupUI()
		
		addContentView(contentView: signInView)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.setNavigationBarHidden(true, animated: animated)
	}

}

fileprivate final class SignInContainerView: LoginView {
	
	private struct Appearance {
		static let infoViewHeightAnchorConstant: CGFloat = 175
		static let textFieldsContainerHeightAnchorConstant: CGFloat = 120
		static let textFieldsContainerWidthAnchorConstant: CGFloat = 300
		static let textFieldsContainerTopAnchorConstant: CGFloat = 10
		static let forgotPasswordButtonHeightAnchorConstant: CGFloat = 20
		static let forgotPasswordButtonWidthAnchorConstant: CGFloat = 110
		static let forgotPasswordButtonTopAnchorConstant: CGFloat = 26
		static let loginButtonsBarHeightAnchorConstant: CGFloat = 170
	}
	
	// MARK: - Properties
	
	var forgotPasswordButtonActionHandler: (() -> Void)?
	
	private lazy var textFieldsContainer: UIView = {
		return SignInTextFieldsContainer()
	}()
	
	private lazy var forgotPasswordButton: UIButton = {
		let button = UIButton()
		let text = StyledText(
			text: localizedText(for: .forgotPassword),
			style: TextStyle(color: Colors.lightGray, font: .systemFont(ofSize: 14)))
		button.set(styledText: text)
		button.addTarget(
			self,
			action: #selector(forgotPasswordButtonTapped),
			for: .touchUpInside)
		
		return button
	}()
	
	private lazy var loginButtonsBar: UIView = {
		let buttonsBar = LoginButtonsBar()
		buttonsBar.signUpButtonActionHandler = {
			print(#function)
		}
		buttonsBar.signInButtonActionHandler = {
			print(#function)
		}
		
		return buttonsBar
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		prepareLayout()
	}
	
	private func prepareLayout() {
		addSubviews([textFieldsContainer, forgotPasswordButton, loginButtonsBar])
		
		NSLayoutConstraint.activate(
			[textFieldsContainer.heightAnchor.constraint(
				equalToConstant: Appearance.textFieldsContainerHeightAnchorConstant),
			 textFieldsContainer.widthAnchor.constraint(
				equalToConstant: Appearance.textFieldsContainerWidthAnchorConstant),
			 textFieldsContainer.topAnchor.constraint(
				equalTo: infoView.bottomAnchor,
				constant: Appearance.textFieldsContainerTopAnchorConstant),
			 textFieldsContainer.centerXAnchor.constraint(
				equalTo: infoView.centerXAnchor),
			 forgotPasswordButton.heightAnchor.constraint(
				equalToConstant: Appearance.forgotPasswordButtonHeightAnchorConstant),
			 forgotPasswordButton.widthAnchor.constraint(
				equalToConstant: Appearance.forgotPasswordButtonWidthAnchorConstant),
			 forgotPasswordButton.topAnchor.constraint(
				equalTo: textFieldsContainer.bottomAnchor,
				constant: Appearance.forgotPasswordButtonTopAnchorConstant),
			 forgotPasswordButton.leadingAnchor.constraint(
				equalTo: textFieldsContainer.leadingAnchor),
			 loginButtonsBar.heightAnchor.constraint(
				equalToConstant: Appearance.loginButtonsBarHeightAnchorConstant),
			 loginButtonsBar.bottomAnchor.constraint(
				equalTo: bottomAnchor),
			 loginButtonsBar.leadingAnchor.constraint(
				equalTo: leadingAnchor),
			 loginButtonsBar.trailingAnchor.constraint(
				equalTo: trailingAnchor)])
	}
	
	@objc private func forgotPasswordButtonTapped() {
		forgotPasswordButtonActionHandler?()
	}
	
}

fileprivate final class SignInTextFieldsContainer: BaseView {
	
	// MARK: - Properties
	
	lazy var loginTextField: TextField = {
		let textField = TextField()
		textField.fieldName = localizedText(for: .login)
		textField.clearButtonMode = .whileEditing
		
		return textField
	}()
	
	lazy var passwordTextField: TextField = {
		let textField = TextField()
		textField.fieldName = localizedText(for: .password)
		textField.isSecure = true
		textField.clearButtonMode = .whileEditing
		
		return textField
	}()
	
	private lazy var line: UIView = {
		let line = UIView()
		line.backgroundColor = Colors.lightGray
		
		return line
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		layer.cornerRadius = 13
		layer.borderWidth = 0.5
		layer.borderColor = Colors.lightGray.cgColor
		
		addSubviews([line, loginTextField, passwordTextField])
		NSLayoutConstraint.activate(
			[line.heightAnchor.constraint(equalToConstant: 0.5),
			 line.centerYAnchor.constraint(equalTo: centerYAnchor),
			 line.leadingAnchor.constraint(equalTo: leadingAnchor),
			 line.trailingAnchor.constraint(equalTo: trailingAnchor),
			 loginTextField.topAnchor.constraint(equalTo: topAnchor),
			 loginTextField.leadingAnchor.constraint(equalTo: leadingAnchor),
			 loginTextField.trailingAnchor.constraint(equalTo: trailingAnchor),
			 loginTextField.bottomAnchor.constraint(equalTo: line.topAnchor),
			 passwordTextField.topAnchor.constraint(equalTo: line.bottomAnchor),
			 passwordTextField.heightAnchor.constraint(equalTo: loginTextField.heightAnchor),
			 passwordTextField.leadingAnchor.constraint(equalTo: loginTextField.leadingAnchor),
			 passwordTextField.trailingAnchor.constraint(equalTo: loginTextField.trailingAnchor),
			 passwordTextField.bottomAnchor.constraint(equalTo: bottomAnchor)])
	}
	
}
