//
//  SignUpSetPasswordViewController.swift
//  teremok
//
//  Created by Maximychev Evgeny on 26/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class SignUpSetPasswordViewController: ModalViewController {
	
	// MARK: - Properties
	
	private lazy var signUpSetPasswordView: SignUpSetPasswordView = {
		let view = SignUpSetPasswordView()
		view.backButtonActionHandler = {
			self.navigationController?.popViewController(animated: false)
		}
		let text = StyledText(
			text: "Установите пароль для входа в приложение",
			style: TextStyle(
				color: Colors.lightGray,
				font: .systemFont(ofSize: 14)))
		view.infoView.infoLabel.set(styledText: text)
		view.button.addTarget(
			self,
			action: #selector(buttonTapped),
			for: .touchUpInside)
		
		return view
	}()
	
	// MARK: - Override
	
	override func setupUI() {
		super.setupUI()
		
		addContentView(contentView: signUpSetPasswordView)
	}
	
	// MARK: - Actions
	
	@objc private func buttonTapped() {
		navigationController?.pushViewController(
			TermsOfUseViewController(),
			animated: false)
	}

}

fileprivate final class SignUpSetPasswordView: LoginView {
	
	private struct Appearance {
		static let infoViewHeightAnchorConstant: CGFloat = 175
		static let passwordFieldHeightAnchorConstant: CGFloat = 59
		static let passwordFieldWidthAnchorConstant: CGFloat = 300
		static let passwordFieldTopAnchorConstant: CGFloat = 16
		static let buttonHeightAnchorConstant: CGFloat = 50
		static let buttonWidthAnchorConstant: CGFloat = 300
		static let buttonTopAnchorConstant: CGFloat = 24
	}
	
	// MARK: - Properties
	
	lazy var passwordField: TextField = {
		let textField = TextField()
		textField.fieldName = localizedText(for: .password)
		textField.isSecure = true
		textField.clearButtonMode = .whileEditing
		textField.layer.cornerRadius = 13
		textField.layer.borderWidth = 0.5
		textField.layer.borderColor = Colors.lightGray.cgColor
		
		return textField
	}()
	
	lazy var button: RoundedButton = {
		let button = RoundedButton()
		button.set(styledText: StyledText(
			text: "Далее",
			style: TextStyle(
				color: .white,
				font: .systemFont(ofSize: 15))))
		
		return button
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		prepareLayout()
	}
	
	// MARK: - Private
	
	private func prepareLayout() {
		addSubviews([passwordField, button])
		
		NSLayoutConstraint.activate(
			[passwordField.heightAnchor.constraint(
				equalToConstant: Appearance.passwordFieldHeightAnchorConstant),
			 passwordField.widthAnchor.constraint(
				equalToConstant: Appearance.passwordFieldWidthAnchorConstant),
			 passwordField.topAnchor.constraint(
				equalTo: infoView.bottomAnchor,
				constant: Appearance.passwordFieldTopAnchorConstant),
			 passwordField.centerXAnchor.constraint(
				equalTo: infoView.centerXAnchor),
			 button.heightAnchor.constraint(
					equalToConstant: Appearance.buttonHeightAnchorConstant),
			 button.widthAnchor.constraint(
				equalToConstant: Appearance.buttonWidthAnchorConstant),
			 button.topAnchor.constraint(
				equalTo: passwordField.bottomAnchor,
				constant: Appearance.buttonHeightAnchorConstant),
			 button.centerXAnchor.constraint(
				equalTo: passwordField.centerXAnchor)])
	}
	
}
