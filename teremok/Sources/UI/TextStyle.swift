//
//  TextStyle.swift
//  teremok
//
//  Created by Maximychev Evgeny on 21/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

// MARK: - Structs

struct TextStyle {
	let color: UIColor
	let font: UIFont
}

struct StyledText {
	let text: String
	let style: TextStyle
}

// MARK: - Protocols

protocol TextStyling {
	func set(styledText: StyledText)
	func set(textStyle: TextStyle)
}

// MARK: - Extensions

extension UILabel: TextStyling {
	func set(styledText: StyledText) {
		text = styledText.text
		set(textStyle: styledText.style)
	}
	
	func set(textStyle: TextStyle) {
		textColor = textStyle.color
		font = textStyle.font
	}
}

extension UITextField: TextStyling {
	func set(styledText: StyledText) {
		text = styledText.text
		set(textStyle: styledText.style)
	}
	
	func set(textStyle: TextStyle) {
		textColor = textStyle.color
		font = textStyle.font
	}
}

extension UITextView: TextStyling {
	func set(styledText: StyledText) {
		text = styledText.text
		set(textStyle: styledText.style)
	}
	
	func set(textStyle: TextStyle) {
		textColor = textStyle.color
		font = textStyle.font
	}
}

extension UIButton: TextStyling {
	func set(styledText: StyledText) {
		setTitle(styledText.text, for: .normal)
		set(textStyle: styledText.style)
	}
	
	func set(textStyle: TextStyle) {
		setTitleColor(textStyle.color, for: .normal)
		titleLabel?.font = textStyle.font
	}
}
