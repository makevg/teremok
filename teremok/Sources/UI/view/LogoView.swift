//
//  LogoView.swift
//  teremok
//
//  Created by Maximychev Evgeny on 24/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class LogoView: BaseView {
	
	private struct Appearance {
		static let logoImageViewHeightAnchorConstant: CGFloat = 40
		static let logoImageViewWidthAnchorConstant: CGFloat = 44
		static let logoImageViewTopAnchorConstant: CGFloat = 28
		static let logoLabelTopAnchorConstant: CGFloat = 4
	}
	
	// MARK: - Properties
	
	private lazy var logoImageView: UIImageView = {
		let imageView = UIImageView(
			image: UIImage(named: "logo"),
			highlightedImage: nil)
		imageView.contentMode = .scaleAspectFit
		
		return imageView
	}()
	
	private lazy var logoLabel: UILabel = {
		let label = UILabel()
		let text = StyledText(
			text: localizedText(for: .appName),
			style: TextStyle(
				color: Colors.brown,
				font: .systemFont(ofSize: 18, weight: .medium)))
		label.set(styledText: text)
		
		return label
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		addSubviews([logoImageView, logoLabel])
		
		NSLayoutConstraint.activate(
			[logoImageView.heightAnchor.constraint(
				equalToConstant: Appearance.logoImageViewHeightAnchorConstant),
			 logoImageView.widthAnchor.constraint(
				equalToConstant: Appearance.logoImageViewWidthAnchorConstant),
			 logoImageView.centerXAnchor.constraint(
				equalTo: centerXAnchor),
			 logoImageView.topAnchor.constraint(
				equalTo: topAnchor,
				constant: Appearance.logoImageViewTopAnchorConstant),
			 logoLabel.topAnchor.constraint(
				equalTo: logoImageView.bottomAnchor,
				constant: Appearance.logoLabelTopAnchorConstant),
			 logoLabel.centerXAnchor.constraint(
				equalTo: logoImageView.centerXAnchor)])
	}

}
