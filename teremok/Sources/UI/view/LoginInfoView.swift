//
//  LoginInfoView.swift
//  teremok
//
//  Created by Maximychev Evgeny on 25/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class LoginInfoView: BaseView {
	
	private struct Appearance {
		static let logoViewHeightConstraint: CGFloat = 95
		static let descrLabelTopAnchorConstant: CGFloat = 4
		static let descrLabelWidthAnchorConstant: CGFloat = 170
		static let infoLabelTopAnchorConstant: CGFloat = 16
	}
	
	// MARK: - Properties
	
	private lazy var logoView: LogoView = {
		return LogoView()
	}()
	
	lazy var descrLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 2
		label.textAlignment = .center
		let text = StyledText(
			text: localizedText(for: .signInDescription),
			style: TextStyle(
				color: Colors.dark,
				font: .systemFont(ofSize: 15)))
		label.set(styledText: text)
		
		return label
	}()
	
	lazy var infoLabel: UILabel = {
		let label = UILabel()
		let text = StyledText(
			text: localizedText(for: .signInInfo),
			style: TextStyle(
				color: Colors.lightGray,
				font: .systemFont(ofSize: 14)))
		label.set(styledText: text)
		
		return label
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		addSubviews([logoView, descrLabel, infoLabel])
		
		NSLayoutConstraint.activate(
			[logoView.heightAnchor.constraint(
				equalToConstant: Appearance.logoViewHeightConstraint),
			 logoView.topAnchor.constraint(
				equalTo: topAnchor),
			 logoView.leadingAnchor.constraint(
				equalTo: leadingAnchor),
			 logoView.trailingAnchor.constraint(
				equalTo: trailingAnchor),
			 descrLabel.topAnchor.constraint(
				equalTo: logoView.bottomAnchor,
				constant: Appearance.descrLabelTopAnchorConstant),
			 descrLabel.centerXAnchor.constraint(
				equalTo: logoView.centerXAnchor),
			 descrLabel.widthAnchor.constraint(
				equalToConstant: Appearance.descrLabelWidthAnchorConstant),
			 infoLabel.topAnchor.constraint(
				equalTo: descrLabel.bottomAnchor,
				constant: Appearance.infoLabelTopAnchorConstant),
			 infoLabel.centerXAnchor.constraint(
				equalTo: descrLabel.centerXAnchor)])
	}

}
