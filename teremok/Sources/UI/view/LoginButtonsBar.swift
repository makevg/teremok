//
//  LoginButtonsBar.swift
//  teremok
//
//  Created by Maximychev Evgeny on 21/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class LoginButtonsBar: BaseView {
	
	private struct Appearance {
		static let signInButtonHeightAnchorConstant: CGFloat = 50
		static let signInButtonWidthAnchorConstant: CGFloat = 300
		static let signInButtonTopAnchorConstant: CGFloat = 30
		static let signUpButtonHeightAnchorConstant: CGFloat = 16
		static let signUpButtonWidthAnchorConstant: CGFloat = 120
		static let signUpButtonTopAnchorConstant: CGFloat = 27
	}
	
	// MARK: - Properties
	
	var signUpButtonActionHandler: (() -> Void)?
	var signInButtonActionHandler: (() -> Void)?
	
	private lazy var signInButton: RoundedButton = {
		let button = RoundedButton()
		button.set(styledText: StyledText(
			text: localizedText(for: .signInButtonTitle),
			style: TextStyle(color: .white, font: .systemFont(ofSize: 15))))
		button.addTarget(
			self,
			action: #selector(signInButtonTapped),
			for: .touchUpInside)
		
		return button
	}()
	
	private lazy var signUpButton: UIButton = {
		let button = UIButton()
		button.backgroundColor = .white
		button.set(styledText: StyledText(
			text: localizedText(for: .signUpButtonTitle),
			style: TextStyle(
				color: Colors.brown,
				font: .systemFont(ofSize: 15))))
		button.addTarget(
			self,
			action: #selector(signUpButtonTapped),
			for: .touchUpInside)
		
		return button
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		backgroundColor = .white
		layer.cornerRadius = 35
		addSubviews([signInButton, signUpButton])
		
		NSLayoutConstraint.activate(
			[signInButton.heightAnchor.constraint(
				equalToConstant: Appearance.signInButtonHeightAnchorConstant),
			 signInButton.widthAnchor.constraint(
				equalToConstant: Appearance.signInButtonWidthAnchorConstant),
			 signInButton.centerXAnchor.constraint(
				equalTo: centerXAnchor),
			 signInButton.topAnchor.constraint(
				equalTo: topAnchor,
				constant: Appearance.signInButtonTopAnchorConstant),
			 signUpButton.heightAnchor.constraint(
				equalToConstant: Appearance.signUpButtonHeightAnchorConstant),
			 signUpButton.widthAnchor.constraint(
				equalToConstant: Appearance.signUpButtonWidthAnchorConstant),
			 signUpButton.centerXAnchor.constraint(
				equalTo: signInButton.centerXAnchor),
			 signUpButton.topAnchor.constraint(
				equalTo: signInButton.bottomAnchor,
				constant: Appearance.signUpButtonTopAnchorConstant)])
	}
	
	// MARK: - Actions
	
	@objc private func signUpButtonTapped() {
		signUpButtonActionHandler?()
	}
	
	@objc private func signInButtonTapped() {
		signInButtonActionHandler?()
	}
	
}
