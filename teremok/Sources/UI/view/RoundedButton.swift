//
//  RoundedButton.swift
//  teremok
//
//  Created by Maximychev Evgeny on 25/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
	
	// MARK: - Init
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		configureSubviews()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		configureSubviews()
	}
	
	// MARK: - Private
	
	private func configureSubviews() {
		layer.cornerRadius = 13
		backgroundColor = Colors.brown
	}
	
}
