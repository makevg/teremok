//
//  TextField.swift
//  teremok
//
//  Created by Maximychev Evgeny on 22/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

class TextField: BaseView {
	
	private struct Appearance {
		static let textLabelLeadingAnchorConstant: CGFloat = 20
		static let textLabelWidthAnchorConstant: CGFloat = 60
		static let textFieldLeadingAnchorConstant: CGFloat = 8
		static let textFieldTrailingAnchorConstant: CGFloat = -8
	}
	
	// MARK: - Properties
	
	var fieldName: String = "" {
		didSet { textLabel.text = fieldName }
	}
	
	var fieldText: String = "" {
		didSet { textField.text = fieldText }
	}
	
	var isSecure: Bool = false {
		didSet { textField.isSecureTextEntry = isSecure }
	}
	
	var clearButtonMode: UITextField.ViewMode = .never {
		didSet { textField.clearButtonMode = clearButtonMode }
	}
	
	var errorState: Bool = false {
		didSet {
			let textColor: UIColor = errorState ? .red : .gray
			textLabel.textColor = textColor
		}
	}
	
	private lazy var textLabel: UILabel = {
		let label = UILabel()
		label.textColor = .gray
		
		return label
	}()
	
	private lazy var textField: UITextField = {
		return UITextField()
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		addSubviews([textLabel, textField])
		
		NSLayoutConstraint.activate(
			[textLabel.centerYAnchor.constraint(
				equalTo: centerYAnchor),
			 textLabel.leadingAnchor.constraint(
				equalTo: leadingAnchor,
				constant: Appearance.textLabelLeadingAnchorConstant),
			 textLabel.widthAnchor.constraint(
				equalToConstant: Appearance.textLabelWidthAnchorConstant),
			 textField.leadingAnchor.constraint(
				equalTo: textLabel.trailingAnchor,
				constant: Appearance.textFieldLeadingAnchorConstant),
			 textField.trailingAnchor.constraint(
				equalTo: trailingAnchor,
				constant: Appearance.textFieldTrailingAnchorConstant),
			 textField.topAnchor.constraint(
				equalTo: topAnchor),
			 textField.bottomAnchor.constraint(
				equalTo: bottomAnchor)])
	}

}
