//
//  PhoneField.swift
//  teremok
//
//  Created by Maximychev Evgeny on 24/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class PhoneField: BaseView {
	
	private struct Appearance {
		static let textLabelWidthAnchorConstant: CGFloat = 50
		static let lineWidthAnchorConstant: CGFloat = 0.5
		static let textFieldTrailingAnchorConstant: CGFloat = -8
	}
	
	// MARK: - Properties
	
	private lazy var textLabel: UILabel = {
		let label = UILabel()
		label.text = "+7"
		label.textColor = .gray
		label.textAlignment = .center
		
		return label
	}()
	
	private let line: UIView = {
		let line = UIView()
		line.backgroundColor = .lightGray
		
		return line
	}()
	
	private lazy var textField: UITextField = {
		let textField = UITextField()
		textField.textAlignment = .center
		textField.clearButtonMode = .whileEditing
		textField.placeholder = "Номер телефона"
		
		return textField
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		layer.cornerRadius = 13
		layer.borderWidth = 0.5
		layer.borderColor = UIColor.lightGray.cgColor
		
		addSubviews([textLabel, line, textField])
		
		NSLayoutConstraint.activate(
			[textLabel.topAnchor.constraint(
				equalTo: topAnchor),
			 textLabel.bottomAnchor.constraint(
				equalTo: bottomAnchor),
			 textLabel.leadingAnchor.constraint(
				equalTo: leadingAnchor),
			 textLabel.widthAnchor.constraint(
				equalToConstant: Appearance.textLabelWidthAnchorConstant),
			 line.widthAnchor.constraint(
				equalToConstant: Appearance.lineWidthAnchorConstant),
			 line.leadingAnchor.constraint(
				equalTo: textLabel.trailingAnchor),
			 line.topAnchor.constraint(
				equalTo: topAnchor),
			 line.bottomAnchor.constraint(
				equalTo: bottomAnchor),
			 textField.topAnchor.constraint(
				equalTo: topAnchor),
			 textField.bottomAnchor.constraint(
				equalTo: bottomAnchor),
			 textField.leadingAnchor.constraint(
				equalTo: line.trailingAnchor),
			 textField.trailingAnchor.constraint(
				equalTo: trailingAnchor,
				constant: Appearance.textFieldTrailingAnchorConstant)])
	}

}
