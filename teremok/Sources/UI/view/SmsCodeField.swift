//
//  SmsCodeField.swift
//  teremok
//
//  Created by Maximychev Evgeny on 25/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

final class SmsCodeField: BaseView, UITextFieldDelegate {
	
	private struct Constants {
		static let fieldsCount: Int = 4
	}
	
	// MARK: - Properties
	
	var code: String {
		set {
			for (index, field) in fields.enumerated() {
				let index = newValue.index(newValue.startIndex, offsetBy: index)
				field.text = String(newValue[index])
			}
		}
		get {
			return fields.map { $0.code }.reduce("", +)
		}
	}
	
	var errorState: Bool = false {
		didSet {
			let borderColor: UIColor = errorState ? Colors.red : Colors.lightGray
			fields.forEach { $0.layer.borderColor = borderColor.cgColor }
		}
	}
	
	var codeEnteredHandler: ((String) -> Void)?
	
	private var fields: [UITextField] {
		return container.arrangedSubviews as? [UITextField] ?? [UITextField]()
	}
	
	private lazy var container: UIStackView = {
		let container = UIStackView()
		container.axis = .horizontal
		container.distribution = .fillEqually
		container.spacing = 13
		
		for index in 0..<Constants.fieldsCount {
			container.addArrangedSubview(
				createNumberField(tag: index))
		}
		
		return container
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		addSubviews([container])
		
		NSLayoutConstraint.activate(
			[container.topAnchor.constraint(equalTo: topAnchor),
			 container.bottomAnchor.constraint(equalTo: bottomAnchor),
			 container.leadingAnchor.constraint(equalTo: leadingAnchor),
			 container.trailingAnchor.constraint(equalTo: trailingAnchor)])
	}
	
	// MARK: - Private
	
	private func createNumberField(tag: Int) -> UITextField {
		let field = UITextField()
		field.textColor = .black
		field.textAlignment = .center
		field.font = .systemFont(ofSize: 22)
		field.layer.cornerRadius = 13
		field.layer.borderWidth = 1
		field.layer.borderColor = Colors.lightGray.cgColor
		field.delegate = self
		field.tag = tag
		field.addTarget(
			self,
			action: #selector(textFieldDidChange(_:)),
			for: .editingChanged)
		
		return field
	}
	
	// MARK: - Actions
	
	@objc private func textFieldDidChange(_ textField: UITextField) {
		let fieldTag = textField.tag
		let fieldsCount = Constants.fieldsCount
		
		guard fieldTag == fieldsCount - 1, code.count == fieldsCount else {
			let index = textField.code.isEmpty ? fieldTag - 1 : fieldTag + 1
			if index >= 0 && index < Constants.fieldsCount {
				fields[index].becomeFirstResponder()
			}
			
			return
		}
		
		codeEnteredHandler?(code)
	}
	
	// MARK: - UITextFieldDelegate
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		let currentText = textField.text ?? ""
		guard let stringRange = Range(range, in: currentText) else { return false }
		
		let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
		
		return updatedText.count <= 1
	}

}

// MARK: - Extensions

private extension UITextField {
	var code: String { return text ?? "" }
}
