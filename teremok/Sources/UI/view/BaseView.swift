//
//  BaseView.swift
//  teremok
//
//  Created by Максимычев Е.О. on 26/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

class BaseView: UIView {
	
	// MARK: - Init
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		configureSubviews()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		configureSubviews()
	}
	
	// MARK: - Configure
	
	func configureSubviews() {
		
	}

}
