//
//  LoginView.swift
//  teremok
//
//  Created by Максимычев Е.О. on 27/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

class LoginView: BaseView {
	
	private struct Appearance {
		static let backButtonSize: CGFloat = 32
		static let backButtonTopAnchorConstant: CGFloat = 28
		static let backButtonLeadingAnchorConstant: CGFloat = 36
		static let infoViewHeightAnchorConstant: CGFloat = 175
		static let infoViewWidthAnchorConstant: CGFloat = 200
	}
	
	// MARK: - Properties
	
	var backButtonIsHidden: Bool = false {
		didSet { backButton.isHidden = backButtonIsHidden  }
	}
	
	var backButtonActionHandler: (() -> Void)?
	
	private lazy var backButton: UIButton = {
		let button = UIButton()
		button.setImage(UIImage(named: "Back"), for: .normal)
		button.layer.cornerRadius = Appearance.backButtonSize / 2
		button.layer.borderColor = Colors.lightGray.cgColor
		button.layer.borderWidth = 0.5
		button.addTarget(
			self,
			action: #selector(backButtonTapped),
			for: .touchUpInside)
		
		return button
	}()
	
	lazy var infoView: LoginInfoView = {
		let view = LoginInfoView()
		
		return view
	}()
	
	// MARK: - Override
	
	override func configureSubviews() {
		super.configureSubviews()
		
		prepareLayout()
		setupTapGestureRecognizer()
	}
	
	// MARK: - Private
	
	private func prepareLayout() {
		addSubviews([backButton, infoView])
		
		NSLayoutConstraint.activate(
			[backButton.heightAnchor.constraint(
				equalToConstant: Appearance.backButtonSize),
			 backButton.widthAnchor.constraint(
				equalTo: backButton.heightAnchor),
			 backButton.topAnchor.constraint(
				equalTo: topAnchor,
				constant: Appearance.backButtonTopAnchorConstant),
			 backButton.leadingAnchor.constraint(
				equalTo: leadingAnchor,
				constant: Appearance.backButtonLeadingAnchorConstant),
			 infoView.heightAnchor.constraint(
				equalToConstant: Appearance.infoViewHeightAnchorConstant),
			 infoView.widthAnchor.constraint(
				equalToConstant: Appearance.infoViewWidthAnchorConstant),
			 infoView.centerXAnchor.constraint(
				equalTo: centerXAnchor),
			 infoView.topAnchor.constraint(
				equalTo: topAnchor)])
	}
	
	private func setupTapGestureRecognizer() {
		let tapGestureRecognizer = UITapGestureRecognizer(
			target: self,
			action: #selector(viewTapped))
		addGestureRecognizer(tapGestureRecognizer)
	}
	
	// MARK: - Actions
	
	@objc private func viewTapped() {
		endEditing(true)
	}
	
	@objc private func backButtonTapped() {
		backButtonActionHandler?()
	}

}
