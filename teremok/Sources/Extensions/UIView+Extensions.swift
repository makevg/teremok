//
//  UIView+Extensions.swift
//  teremok
//
//  Created by Maximychev Evgeny on 20/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import UIKit

extension UIView {
	
	func addSubviews(_ subviews: [UIView]) {
		subviews.forEach {
			addSubview($0)
			$0.translatesAutoresizingMaskIntoConstraints = false
		}
	}
	
}
