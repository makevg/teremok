//
//  Localization.swift
//  teremok
//
//  Created by Maximychev Evgeny on 21/08/2019.
//  Copyright © 2019 van.work. All rights reserved.
//

import Foundation

enum LocalizationKey: String {
	case appName = "AppName"
	case login = "Login"
	case password = "Password"
	case forgotPassword = "ForgotPassword"
	case firstHomePageTitle = "Home.FirstPage.Title"
	case firstHomePageDescription = "Home.FirstPage.Description"
	case secondHomePageTitle = "Home.SecondPage.Title"
	case secondHomePageDescription = "Home.SecondPage.Description"
	case thirdHomePageTitle = "Home.ThirdPage.Title"
	case thirdHomePageDescription = "Home.ThirdPage.Description"
	case signUpButtonTitle = "SignUpButton.Title"
	case signInButtonTitle = "SignInButton.Title"
	case signInDescription = "SignIn.Description"
	case signInInfo = "SignIn.Info"
	case phoneNumber = "SignUp.PhoneNumber"
	case codeSendToNumber = "SignUp.CodeSendToNumber"
	case dontReceiveCode = "SignUp.DontReceiveCode"
	case resendCode = "SignUp.ResendCode"
	case hasAccount = "SignUp.HasAccount"
	case enterPhoneNumber = "SignUp.EnterPhoneNumber"
	case acceptPersonalDataProcessing = "SignUp.AcceptPersonalDataProcessing"
	case termsOfUseTitle = "TermsOfUse.Title"
	case termsOfUseDescription = "TermsOfUse.Description"
	case termsOfUseAcceptButtonTitle = "TermsOfUse.AcceptButtonTitle"
}

func localizedText(for key: LocalizationKey) -> String {
	return NSLocalizedString(key.rawValue, comment: "")
}
